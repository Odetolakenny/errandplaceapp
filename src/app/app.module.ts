import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ChoosePaymentTypePage } from '../pages/choose-payment-type/choose-payment-type';
import { ChooseErrandPage } from '../pages/choose-errand/choose-errand';
import { ConfirmPaylaterPage } from '../pages/confirm-paylater/confirm-paylater';
import { ConfirmPayonlinePage } from '../pages/confirm-payonline/confirm-payonline';
import { PayPage } from '../pages/pay/pay';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChooseErrandPage,
    ChoosePaymentTypePage,
    ConfirmPaylaterPage,
    ConfirmPayonlinePage,
    PayPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChooseErrandPage,
    ChoosePaymentTypePage,
    ConfirmPaylaterPage,
    ConfirmPayonlinePage,
    PayPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
