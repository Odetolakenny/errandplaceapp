import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BecomePatnerPage } from './become-patner';

@NgModule({
  declarations: [
    BecomePatnerPage,
  ],
  imports: [
    IonicPageModule.forChild(BecomePatnerPage),
  ],
})
export class BecomePatnerPageModule {}
