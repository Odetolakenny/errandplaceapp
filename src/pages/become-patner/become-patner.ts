import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-become-patner',
  templateUrl: 'become-patner.html',
})
export class BecomePatnerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}
