import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChooseErrandPage } from './choose-errand';

@NgModule({
  declarations: [
    ChooseErrandPage,
  ],
  imports: [
    IonicPageModule.forChild(ChooseErrandPage),
  ],
})
export class ChooseErrandPageModule {}
