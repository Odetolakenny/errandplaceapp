import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ChoosePaymentTypePage } from '../choose-payment-type/choose-payment-type';

@IonicPage()
@Component({
  selector: 'page-choose-errand',
  templateUrl: 'choose-errand.html',
})
export class ChooseErrandPage {

  public from1: any = "Surulere,Lagos State";
  public from2: any = "Ipaja,Lagos State";
  public from3: any = "Lagos State";
  public from4: any = "Oshodi,Lagos State";
  public from5: any = "Lekki,Lagos State";

  public to1: any = "Mainland,Lagos State";
  public to2: any = "Somolu,Lagos State";
  public to3: any = "Lagos State";
  public to4: any = "Lekki,Lagos State";
  public to5: any = "Ajah,Lagos State";

  public amount1: any = "NGN 500";
  public amount2: any = "NGN 500";
  public amount3: any = "NGN 1500";
  public amount4: any = "NGN 1800";
  public amount5: any = "NGN 3800";

  public using1: any = "OPELOGIC";
  public using2: any = "LG LOGISTICS";
  public using3: any = "MUSTAZ";
  public using4: any = "MUSTAZ";
  public using5: any = "MUSTAZ";

  public processing1: any = "Same Day Delivery";
  public processing2: any = "5 - 7 day(s)";
  public processing3: any = "1 - 7 day(s)";
  public processing4: any = "1 - 7 day(s)";
  public processing5: any = "1 - 7 day(s)";

  public mode1: any = "Doorstep pickup";
  public mode2: any = "Doorstep pickup";
  public mode3: any = "Come drop at our office";
  public mode4: any = "Doorstep pickup";
  public mode5: any = "Doorstep pickup";

  public weight1: any = "2 - 2 kg";
  public weight2: any = "2 - 10 kg";
  public weight3: any = "1 - 3 kg";
  public weight4: any = "1 - 3 kg";
  public weight5: any = "1 - 3 kg";
  
  public important1: any = " ";
  public important2: any = "Delivery within the Lagos Island";
  public important3: any = "Please drop item at our office at 9, Ikeja Road";
  public important4: any = " ";
  public important5: any = "We run interstate errands";

  errand1(from:string,toErrand:string,amount:string){
    let prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Enter the following details",
      inputs: [
        {
          name: 'Pickup Address',
          placeholder: 'Input pickup address as best as possible'
        },
        {
          name: 'Pickup contact Telephone',
          placeholder: 'Pickup contact Telephone'
        },
        {
          name: 'Destination Address',
          placeholder: 'Input destination address as best as possible'
        },
        {
          name: "Receiver's contact Telephone",
          placeholder: 'Receivers contact Telephone'
        },
        {
          name: "Remarks",
          placeholder: 'Remarks'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            this.navCtrl.push(ChoosePaymentTypePage, {from: this.from1,toErrand: this.to1,amount: this.amount1});
          }
        }
      ]
    });
    prompt.present();
  }

  errand2(from:string,toErrand:string,amount:string){
    let prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Enter the following details",
      inputs: [
        {
          name: 'Pickup Address',
          placeholder: 'Input pickup address as best as possible'
        },
        {
          name: 'Pickup contact Telephone',
          placeholder: 'Pickup contact Telephone'
        },
        {
          name: 'Destination Address',
          placeholder: 'Input destination address as best as possible'
        },
        {
          name: "Receiver's contact Telephone",
          placeholder: 'Receivers contact Telephone'
        },
        {
          name: "Remarks",
          placeholder: 'Remarks'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            this.navCtrl.push(ChoosePaymentTypePage, {from: this.from2,toErrand: this.to2,amount: this.amount2});
          }
        }
      ]
    });
    prompt.present();
  }

  errand3(from:string,toErrand:string,amount:string){
    let prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Enter the following details",
      inputs: [
        {
          name: 'Pickup Address',
          placeholder: 'Input pickup address as best as possible'
        },
        {
          name: 'Pickup contact Telephone',
          placeholder: 'Pickup contact Telephone'
        },
        {
          name: 'Destination Address',
          placeholder: 'Input destination address as best as possible'
        },
        {
          name: "Receiver's contact Telephone",
          placeholder: 'Receivers contact Telephone'
        },
        {
          name: "Remarks",
          placeholder: 'Remarks'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            this.navCtrl.push(ChoosePaymentTypePage, {from: this.from3,toErrand: this.to3,amount: this.amount3});
          }
        }
      ]
    });
    prompt.present();
  }

  errand4(from:string,toErrand:string,amount:string){
    let prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Enter the following details",
      inputs: [
        {
          name: 'Pickup Address',
          placeholder: 'Input pickup address as best as possible'
        },
        {
          name: 'Pickup contact Telephone',
          placeholder: 'Pickup contact Telephone'
        },
        {
          name: 'Destination Address',
          placeholder: 'Input destination address as best as possible'
        },
        {
          name: "Receiver's contact Telephone",
          placeholder: 'Receivers contact Telephone'
        },
        {
          name: "Remarks",
          placeholder: 'Remarks'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            this.navCtrl.push(ChoosePaymentTypePage, {from: this.from4,toErrand: this.to4,amount: this.amount4});
          }
        }
      ]
    });
    prompt.present();
  }

  errand5(from:string,toErrand:string,amount:string){
    let prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Enter the following details",
      inputs: [
        {
          name: 'Pickup Address',
          placeholder: 'Input pickup address as best as possible'
        },
        {
          name: 'Pickup contact Telephone',
          placeholder: 'Pickup contact Telephone'
        },
        {
          name: 'Destination Address',
          placeholder: 'Input destination address as best as possible'
        },
        {
          name: "Receiver's contact Telephone",
          placeholder: 'Receivers contact Telephone'
        },
        {
          name: "Remarks",
          placeholder: 'Remarks'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            this.navCtrl.push(ChoosePaymentTypePage, {from: this.from5,toErrand: this.to5,amount: this.amount5});
          }
        }
      ]
    });
    prompt.present();
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
     private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }
}
