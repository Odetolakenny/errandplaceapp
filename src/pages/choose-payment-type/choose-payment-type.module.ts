import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoosePaymentTypePage } from './choose-payment-type';

@NgModule({
  declarations: [
    ChoosePaymentTypePage,
  ],
  imports: [
    IonicPageModule.forChild(ChoosePaymentTypePage),
  ],
})
export class ChoosePaymentTypePageModule {}
