import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ConfirmPayonlinePage } from '../confirm-payonline/confirm-payonline';
import { ConfirmPaylaterPage } from '../confirm-paylater/confirm-paylater';

@IonicPage()
@Component({
  selector: 'page-choose-payment-type',
  templateUrl: 'choose-payment-type.html',
})
export class ChoosePaymentTypePage implements OnInit{


  public contactForm: any;

  public submitAttempt: boolean = false;

  from: any;
  toErrand: any;
  amount: any;
  email: any;
  phoneNumber: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

  
  }
  

  ngOnInit() {
    this.from = this.navParams.get('from');
    this.toErrand = this.navParams.get('toErrand');
    this.amount = this.navParams.get('amount');
  }


  payOnline(amountToPayOnline:string, emailToPayOnline:string, phoneNumberToPayOnline:number){

    if(this.email === null && this.phoneNumber.toString.length != 11){
      let alert = this.alertCtrl.create({
        subTitle: 'Enter valid inputs',
        buttons: ['OK']
      });
      alert.present();
    }else{
      let loader = this.loadingCtrl.create({
        content: "Please wait...",
      });
      loader.present();
      this.navCtrl.push(ConfirmPayonlinePage, {amountToPayOnline: this.amount, emailToPayOnline: this.email,phoneNumberToPayOnline: this.phoneNumber});
      loader.dismiss();
    }
  }

  payLater(amountToPayLater:string,emailToPayLater: string,phoneNumberToPayLater:number){
    if(this.email === null && this.phoneNumber.toString.length != 11){
      let alert = this.alertCtrl.create({
        subTitle: 'Enter valid inputs',
        buttons: ['OK']
      });
      alert.present();
    }else{
      let loader = this.loadingCtrl.create({
        content: "Please wait...",
      });
      loader.present();
      this.navCtrl.push(ConfirmPaylaterPage, {amountToPayLater: this.amount, emailToPayLater: this.email,phoneNumberToPayLater: this.phoneNumber});
      loader.dismiss();
    }
  }
  cancel(){
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.navCtrl.popToRoot();
    loader.dismiss();
  }
}