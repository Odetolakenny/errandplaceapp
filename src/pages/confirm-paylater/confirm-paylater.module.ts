import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmPaylaterPage } from './confirm-paylater';

@NgModule({
  declarations: [
    ConfirmPaylaterPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmPaylaterPage),
  ],
})
export class ConfirmPaylaterPageModule {}
