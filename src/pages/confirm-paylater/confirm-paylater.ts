import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-confirm-paylater',
  templateUrl: 'confirm-paylater.html',
})
export class ConfirmPaylaterPage implements OnInit {

  amountToPayLater: any;
  emailToPayLater: any;
  phoneNumberToPayLater: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController) {
  }

  ngOnInit() {
    this.amountToPayLater = this.navParams.get('amountToPayLater');
    this.emailToPayLater = this.navParams.get('emailToPayLater');
    this.phoneNumberToPayLater = this.navParams.get('phoneNumberToPayLater');
  }
  goToPay(){
    let loader = this.loadingCtrl.create({
      content: "Confirming PayLater",
    });
    loader.present();
    this.navCtrl.push('SuccessPage');
    loader.dismiss();
  }

  goToHome(){
    let loader = this.loadingCtrl.create({
      content: "Cancelling...",
    });
    loader.present();
    this.navCtrl.popToRoot();
    loader.dismiss();
  }
}
