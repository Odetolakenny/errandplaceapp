import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmPayonlinePage } from './confirm-payonline';

@NgModule({
  declarations: [
    ConfirmPayonlinePage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmPayonlinePage),
  ],
})
export class ConfirmPayonlinePageModule {}
