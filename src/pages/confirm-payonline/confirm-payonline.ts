import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { PayPage } from '../pay/pay';

@IonicPage()
@Component({
  selector: 'page-confirm-payonline',
  templateUrl: 'confirm-payonline.html',
})
export class ConfirmPayonlinePage implements OnInit {

  amountToPayOnline: any;
  emailToPayOnline: any;
  phoneNumberToPayOnline: any;

  ngOnInit() {
    this.amountToPayOnline = this.navParams.get('amountToPayOnline');
    this.emailToPayOnline = this.navParams.get('emailToPayOnline');
    this.phoneNumberToPayOnline = this.navParams.get('phoneNumberToPayOnline');
  }

  goToPay(amount:string, email:string){
    let loader = this.loadingCtrl.create({
      content: "Redirecting to payStack...",
    });
    loader.present();
    this.navCtrl.push(PayPage, {amount:this.amountToPayOnline, email:this.emailToPayOnline});
    loader.dismiss();
  }

  goToHome(){
    let loader = this.loadingCtrl.create({
    content: "Cancelling...",
  });
  loader.present();
  this.navCtrl.popToRoot();
  loader.dismiss();
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController) {
  }
}
