import { Component } from '@angular/core';
import { NavController,AlertController,LoadingController } from 'ionic-angular';
import { ChooseErrandPage } from '../choose-errand/choose-errand';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public searchClicked: boolean = true;
  public trackClicked: boolean = false;
  public searchColor: string = 'errand';
  public trackColor: string = 'dark';
  public sourceInput: string;
  public destinationInput: string;
  public size: any;
  
  public searchClick() : void {
    this.searchColor = 'errand'
    this.trackColor = 'dark'
    this.searchClicked = true;
    this.trackClicked = false;
  }

  public trackClick() : void {
    this.trackColor = 'errand'
    this.searchColor = 'dark'
    this.trackClicked = true;
    this.searchClicked = false;
  }

  chooseErrand(){
    if (this.sourceInput === "lagos" && this.destinationInput === "lagos" && this.size === "parcel"){
      let loader = this.loadingCtrl.create({
        content: "Loading...",
      });
     loader.present();
 
     this.navCtrl.push(ChooseErrandPage);
 
     loader.dismiss();
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'For this test mode,<br>Use "lagos" as Source and Destination. Also, choose Parcel as size',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  becomePartner(){
    this.navCtrl.push('BecomePatnerPage');
  }
  goToContact(){
    this.navCtrl.push('ContactPage');
  }

   public sizeGuide(): void{
    let alert = this.alertCtrl.create({
      title: 'Size Guide',
      subTitle: '<p>Parcel: 0.1-2(kg)<br>Small Sized Items: 2.1-5(kg)<br>Medium Sized Items: 5.1-10(kg)<br>Require a Vehicle: 10.1-50(kg)<br>Might require a Van:	50.1-250(kg)<br>Dedicated Long Vehicle:	500.1-1000(kg)<br>Ship and Cargo Shipping:	1000 (kg) and above</p>',
      buttons: ['OK']
    });
    alert.present();
  }

  howitWorks(){
    let alert = this.alertCtrl.create({
      title: 'How it Works',
      subTitle: '<p>Search nearest source and destination.<br>Select estimated size of what to transport. <br> Use our size guide to understand the sizes range values. <br>Check the readme to confirm you agree to our T & C.<br> Click the GO button.<br> Select a Business deal from list of offers.<br> Provide your email address, phone number and Proceed.<br>Errand process starts immediately after.</p>',
      buttons: ['OK']
    });
    alert.present();
  }

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }
}