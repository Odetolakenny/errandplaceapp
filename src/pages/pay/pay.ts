import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html',
})
export class PayPage implements OnInit{

  amount: any;
  email: any;
  public disabled: boolean = true;
  bank: string;
  accountNumber: any;
  otp1: any;
  otp2: any;
  cardNumber: any;
  expiryDate: any;
  cvv: any;
  verifyBank: boolean = true;
  duringVerify: boolean = false;
  afterVerified: boolean = false;

  ngOnInit() {
    this.amount = this.navParams.get('amount');
    this.email = this.navParams.get('email');
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }
  paywithCard(){
  
    if(this.cardNumber === "4084084084084081" && this.cvv === "408"){
      let loader = this.loadingCtrl.create({
        content: "Verifying Card Details...",
      });
      loader.present();
      this.navCtrl.push('SuccessPage');
      loader.dismiss();
    }else{
      let alert = this.alertCtrl.create({
        subTitle: 'Use the values of test details',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  verifyAccount(){
    if(this.bank === "zenith" && this.accountNumber === "0000000000"){
      let loader = this.loadingCtrl.create({
        content: "Verifying Account...",
      });
      loader.present();
      this.verifyBank  = false;
      this.duringVerify  = true;
      loader.dismiss();
    }else{
      let alert = this.alertCtrl.create({
        subTitle: 'Use the values of test details',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  confirmAccount(){
    if(this.otp1 === "123456"){
      let loader = this.loadingCtrl.create({
        content: "Confirming Account...",
      });
      loader.present();
      this.duringVerify  = false;
      this.afterVerified  = true;
      loader.dismiss();
    }else{
      let alert = this.alertCtrl.create({
        subTitle: 'Use the values of test details',
        buttons: ['OK']
      });
      alert.present();
    }

  }

  paywithBank(){
    let loader = this.loadingCtrl.create({
      content: "Paying with Bank...",
    });
    loader.present();
    if(this.otp2 === "123456"){
      this.navCtrl.push('SuccessPage');
      loader.dismiss();
    }else{
      let alert = this.alertCtrl.create({
        subTitle: 'Use the values of test details',
        buttons: ['OK']
      });
      alert.present();
    }
  }
}
